# Настройка семилинков для Open Server #

Структура файлов сайта с многосайтовостью в Open Server:

.  
├── shared  
│   ├── bitrix  
│   └── upload  
│   └── local  
├── site1  
│   ├── ссылка на папку ../shared/bitrix  
│   ├── ссылка на папку ../shared/upload  
│   ├── ссылка на папку ../shared/local  
│   └── ...  
├── site2  
│   ├── ссылка на папку ../shared/bitrix  
│   ├── ссылка на папку ../shared/upload  
│   ├── ссылка на папку ../shared/local  
│   └── ...  

### Порядок действий ###

* создаем папку с ядром сайта (содержит папки: bitrix, upload, images)
* создаем папку с сайтом, котрый необходимо привязать к ядру (site 1)
* создаем домены в панели настройки Open Server
* открываем командную строку windows (cmd)
* Вводим команду на создание семилинка (ссылка на папку bitrix основного ядра):  
  
**mklink /j "site1" "core"**  
  
Пример:  
**mklink /j "C:\OpenServer\domains\site1.loc\bitrix" "C:\OpenServer\domains\core-dev.loc\bitrix"**
  
  
  
  
  